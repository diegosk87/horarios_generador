package proglogica;

import java.util.ArrayList;

/**
 *
 * @author dsk8
 */
public class Empleado {
    private String nombre;
    private boolean turno;
    private boolean sexo;
    private int horas;
    String[] actividades;
    ArrayList<Actividad> acts;

    public Empleado(String nombre, boolean turno, boolean sexo) {
        this.nombre = nombre;
        this.turno = turno;
        this.sexo = sexo;
        this.horas = 0;
        this.actividades = new String[]{"1","2","3","4","5","6"};
        this.acts = new ArrayList<Actividad>();
    }
    public void setNombre(String nom) {
        this.nombre = nom;
    }
    
    public String getNombre() {
        return nombre;
    }

    public boolean getTurno() {
        return turno;
    }

    public boolean getSexo() {
        return sexo;
    }
        
    public int getHoras() {
        return horas;
    }
    
    public void agregarActividad(Actividad actividad) {
        this.acts.add(actividad);
        
        for(int i = horas;i < actividad.getHoras_trabajo() + horas;i++)
            this.actividades[i] = actividad.getNombre();
     
        horas += actividad.getHoras_trabajo();
    }
}
