package proglogica;


import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Brandon Gonzalez
 */
public class VistaEmpleado extends javax.swing.JFrame {

    /**
     * Creates new form Vista
     */
    public Selector sel;
    public String datitos[][] = new String [46][8];
    private String nombre;
    
    public VistaEmpleado(String nombreEmpleado, Selector selector) {
        this.sel = selector;
        initComponents();
        this.nombre = nombreEmpleado;
        this.setTitle(nombre);
        loadDatos();
    }

    public VistaEmpleado(String[][] datos) {
        initComponents();
        this.datitos = datos;
    }
    
    public VistaEmpleado() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Hora", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    //BrandonIn
    private void loadDatos(){
        DefaultTableModel modelo = (DefaultTableModel) this.jTable1.getModel();
        Object x[] = {"Hola", "adios"};
        
        //this.sel = new Selector();
        int filas = modelo.getRowCount();
        int columnas = modelo.getColumnCount();
        this.datitos = sel.getHorarioEmpleado(nombre);
        
        TableColumnModel cmodel = this.jTable1.getColumnModel();
        cmodel.getColumn(0).setMaxWidth(150);
        cmodel.getColumn(0).setMinWidth(100);
        
        this.jTable1.setColumnModel(cmodel);
        
        Font f = new Font("",0,15);
        this.jTable1.setFont(f);
        int con = 0;
        
        int[] pos = new NumeroAle().numero(0, 46);
        String data[][] = new String [46][8];
        
        for (int i = 0; i < pos.length; i++) {
            for (int j = 0; j < 8; j++) {
                data[pos[i]][j] = this.datitos[i][j];
            }
        }
        
        //this.datitos = data;
        
        
        
        for (int i = 0; i < columnas; i++) {
            for (int j = 0; j < filas; j++) {
                modelo.setValueAt(this.datitos[j][i], j, i);
            }
        }
        
        this.jTable1.setModel(modelo);
    }
    //BrandonFIn
    
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VistaEmpleado x = new VistaEmpleado();
                x.setLocationRelativeTo(null);
                x.setExtendedState(JFrame.MAXIMIZED_BOTH);
                x.setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
