package proglogica;

import java.util.ArrayList;

/**
 *
 * @author dsk8
 */
public class Dia {
    private String nombre;
    ArrayList<Empleado> empleados;
    
    public Dia(String nombre) {
        this.nombre = nombre;
        this.empleados = new ArrayList<Empleado>();
    }
    
    public void setEmpleado(Empleado empleado) {
        this.empleados.add(empleado);
    }
    
    public String getNombre() {
        return this.nombre;
    }
}
