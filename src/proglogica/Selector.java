package proglogica;

/**
 *
 * @author dsk8
 */
public class Selector {
    private final int HORAS_TURNO = 6;
    private Dia semana[];
    private Empleado empleados[];
    private Empleado empleado[];
    private Actividad actividades[];
    private Dia random[];
    private NumeroAle nums = new NumeroAle();
    
    private String[] horas= {
        "08:00-09:00","09:00-14:00","08:00-09:00","09:00-14:00","08:00-09:00",
        "09:00-14:00","08:00-12:00","12:00-14:00","08:00-12:00","12:00-14:00",
        "08:00-14:00","08:00-11:00","11:00-14:00","08:00-11:00","11:00-14:00",
        "08:00-10:00","10:00-12:00","12:00-13:00","13:00-14:00","08:00-10:00",
        "10:00-12:00","12:00-13:00","13:00-14:00","14:00-19:00","19:00-20:00",
        "14:00-19:00","19:00-20:00","14:00-19:00","19:00-20:00","14:00-16:00",
        "16:00-20:00","14:00-18:00","18:00-20:00","14:00-20:00","14:00-17:00",
        "17:00-20:00","14:00-17:00","17:00-20:00","14:00-15:00","15:00-16:00",
        "14:00-18:00","18:00-20:00","14:00-15:00","15:00-16:00","16:00-18:00",
        "18:00-20:00"	
    };
    
    public Selector() {
        
        this.semana = new Dia[] {
            new Dia("Lunes"),
            new Dia("Martes"),
            new Dia("Miercoles"),
            new Dia("Jueves"),
            new Dia("Viernes"),
            new Dia("Sabado")
        };
        
        this.empleados = new Empleado[] {
            new Empleado("Humberto Hernandez", true, true), //0
            new Empleado("Juan Garcia", true, true),        //1
            new Empleado("Roberto Aparicio", true, true),   //2
            new Empleado("Joaquin Beltran", true, true),    //3
            new Empleado("Jair Quevedo", true, true),       //4
            new Empleado("Regina Garcia", true, false),     //5
            new Empleado("Brenda Galicia", true, false),    //6
            new Empleado("Sara Connor", true, false),       //7
            new Empleado("Liliana Gonzales", true, false),  //8
            new Empleado("Maria Juarez", true, false),      //9
            new Empleado("Ricardo Dominguez", false, true), //10
            new Empleado("David Moreno", false, true),      //11
            new Empleado("Alberto Palma", false, true),     //12
            new Empleado("Ezequiel Merida", false, true),   //13
            new Empleado("Alan Saldana", false, true),      //14
            new Empleado("Sarai Torres", false, false),     //15
            new Empleado("Alejandra Cruz", false, false),   //16
            new Empleado("Selena Martinez", false, false),  //17
            new Empleado("Viviana Flores", false, false),   //18
            new Empleado("Esther Rojas", false, false)      //19
        };
        
        this.empleado = new Empleado[] {
            new Empleado("Humberto Hernandez", true, true), //0
            new Empleado("Juan Garcia", true, true),        //1
            new Empleado("Roberto Aparicio", true, true),   //2
            new Empleado("Joaquin Beltran", true, true),    //3
            new Empleado("Jair Quevedo", true, true),       //4
            new Empleado("Regina Garcia", true, false),     //5
            new Empleado("Brenda Galicia", true, false),    //6
            new Empleado("Sara Connor", true, false),       //7
            new Empleado("Liliana Gonzales", true, false),  //8
            new Empleado("Maria Juarez", true, false),      //9
            new Empleado("Ricardo Dominguez", false, true), //10
            new Empleado("David Moreno", false, true),      //11
            new Empleado("Alberto Palma", false, true),     //12
            new Empleado("Ezequiel Merida", false, true),   //13
            new Empleado("Alan Saldana", false, true),      //14
            new Empleado("Sarai Torres", false, false),     //15
            new Empleado("Alejandra Cruz", false, false),   //16
            new Empleado("Selena Martinez", false, false),  //17
            new Empleado("Viviana Flores", false, false),   //18
            new Empleado("Esther Rojas", false, false)      //19
        };
        
        this.actividades = new Actividad[] {
            new Actividad("Limpieza de oficinas.", 1),
            new Actividad("Registro y archivo de correspondencia.", 1),
            new Actividad("Recoger la basura.", 1),
            new Actividad("Podar jardines.", 2),
            new Actividad("Podar arboles.", 2),
            new Actividad("Regar areas verdes.", 2),
            new Actividad("Limpieza de sanitarios.", 3),
            new Actividad("Compras.", 3),
            new Actividad("Control y registro.", 6),
            new Actividad("Mantenimiento de vehiculos.", 4),
            new Actividad("Mantenimiento electrico.", 4),
            new Actividad("Mantenimiento hidraulico.", 5),
            new Actividad("Mantenimiento de inmuebles.", 5),
            new Actividad("Mantenimiento de mobiliario.", 5)
        };
        
        generarHorario();
        
    }
    
    public void generarHorario() {
        int i, j, k;
        //boolean paso = true;
        
        for(i = 0;i < 1;i++) {
            for(j = 0;j < 10;j++) {
                if(j < 3) {
                    for(k = 0;k < this.actividades.length ;k++) {
                        if(this.actividades[k].getDisponible()) {
                            if(this.actividades[k].getHoras_trabajo() <= getHorasLibres(this.empleados[j])) {
                                if(!actividadRepetida(this.actividades[k], j)) {
                                    if(!actividadChoca((this.empleados[j].getHoras()), i, this.actividades[k])) {
                                        if(disponibleRegar(this.empleados[j].getHoras(), this.actividades[k])) {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                break;
                                            }
                                            else{
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                break;
                                            }
                                        }
                                        else {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                break;
                                            }
                                            else{
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for(k = this.actividades.length - 1;k >= 0 ;k--) {
                        if(this.actividades[k].getDisponible()) {
                            if(this.actividades[k].getHoras_trabajo() <= getHorasLibres(this.empleados[j])) {
                                if(!actividadRepetida(this.actividades[k], j)) {
                                    if(!actividadChoca((this.empleados[j].getHoras()), i, this.actividades[k])) {
                                        if(disponibleRegar(this.empleados[j].getHoras(), this.actividades[k])) {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                this.actividades[k].setDisponible();
                                            }
                                            else {
                                                this.empleados[j].agregarActividad(this.actividades[k]); 
                                                this.actividades[k].setDisponible();
                                            }
                                        }
                                        else {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                this.actividades[k].setDisponible();
                                            }
                                            else{
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                this.actividades[k].setDisponible();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                else {
                    for(k = this.actividades.length - 1;k >= 0 ;k--) {
                        if(this.actividades[k].getDisponible()) {
                            if(this.actividades[k].getHoras_trabajo() <= getHorasLibres(this.empleados[j])) {
                                if(!actividadRepetida(this.actividades[k], j)) {
                                    if(!actividadChoca((this.empleados[j].getHoras()), i, this.actividades[k])) {
                                        if(this.actividades[k].getDisponible()) {
                                            if(disponibleRegar(this.empleados[j].getHoras(), this.actividades[k])) {
                                                if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                    this.empleados[j].agregarActividad(this.actividades[k]);
                                                    break;
                                                }
                                                else {
                                                    this.empleados[j].agregarActividad(this.actividades[k]);
                                                    break;
                                                }
                                            }
                                            else {
                                                if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                    this.empleados[j].agregarActividad(this.actividades[k]);
                                                    break;
                                                }
                                                else{
                                                    this.empleados[j].agregarActividad(this.actividades[k]);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(this.actividades[k].getNombre().compareTo("Regar areas verdes.") == 0)
                            this.actividades[k].setDisponible();
                    }
                    
                    for(k = this.actividades.length - 1;k >= 0 ;k--) {
                        if(j == 3 || j == 4) {
                            this.actividades[5].setDisponible();
                        }
                        if(this.actividades[k].getDisponible()) {
                            if(this.actividades[k].getHoras_trabajo() <= getHorasLibres(this.empleados[j])) {
                                if(!actividadRepetida(this.actividades[k], j)) {
                                    if(!actividadChoca((this.empleados[j].getHoras()), i, this.actividades[k])) {
                                        if(disponibleRegar(this.empleados[j].getHoras(), this.actividades[k])) {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo())
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                            else
                                                this.empleados[j].agregarActividad(this.actividades[k]);                                               
                                        }
                                        else {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                            }
                                            else {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                this.semana[i].setEmpleado(this.empleados[j]);
            }
        }
        for(i = 0;i < 6;i++) {
            for(j = 10;j < 20;j++) {
                this.semana[i].setEmpleado(this.empleados[j]);
            }
        }
        
        int aux = 10;
        
        for(i = 0;i < 1;i++) {
            for(j = 0;j < 10;j++, aux++) {
                for(k = this.semana[i].empleados.get(j).acts.size() - 1;k >= 0;k--) {
                    this.semana[i].empleados.get(aux).agregarActividad(this.semana[i].empleados.get(j).acts.get(k));
                }
            }
            aux = 10;
        }
        
        Dia n = this.semana[0];
        
        for(i = 0;i < 6;i++) {
            this.semana[i] = n;
        }
        
        this.random = this.semana;
        
        int a[];
        int b;
        
        for(i = 0;i < 6;i++) {
            a = nums.numero(0, 5);
            for(j = 0, b = 0;j < 5;j++, b++) {
                this.random[i].empleados.get(j).setNombre(this.empleado[a[b]].getNombre());
            }
            
            a = nums.numero(5, 10);
            for(j = 5, b = 0;j < 10;j++, b++) {
                this.random[i].empleados.get(j).setNombre(this.empleado[a[b]].getNombre());
            }
            
            a = nums.numero(10, 15);
            for(j = 10, b = 0;j < 15;j++, b++) {
                this.random[i].empleados.get(j).setNombre(this.empleado[a[b]].getNombre());
            }
            
            a = nums.numero(15, 20);
            for(j = 15, b = 0;j < 20;j++, b++) {
                this.random[i].empleados.get(j).setNombre(this.empleado[a[b]].getNombre());
            }
        }
        
        ////////////////
    }
    
    public String[][] getHorario() {
        int i, j, k;
        
        this.semana = this.random;
        
        //a = nums.numero(0, 20);
        
        /*for(k = 0;k < 1;k++) {
            System.out.println("-------------------------------------------------");
            for(j = 0;j < 20;j++) {
                System.out.println(this.semana[k].empleados.get(a[j]).getNombre());
                
                for(i = 0;i < this.semana[i].empleados.get(a[j]).acts.size();i++) {
                    System.out.println(this.semana[i].empleados.get(a[j]).acts.get(i).getNombre());
                }
                
                System.out.println(""); 
            }
        }*/
        
        
        String datos[][] = new String[46][8];
        int cont = 0;
        
        //a = nums.numero(0, 20);
        
        for(k = 0;k < 1;k++) {
            for(j = 0;j < 20;j++) {
                for(i = 0;i < this.semana[k].empleados.get(j).acts.size();i++){
                    //System.out.println(j + " - " + this.semana[k].empleados.get(j).acts.get(i).getNombre());
                    datos[cont][0] = this.horas[cont];
                    datos[cont][1] = this.semana[k].empleados.get(j).acts.get(i).getNombre();
                    cont++;
                }
                    
                //System.out.println(cont); 
            }
            cont=0;
        }
        
        cont = 0;
        
        for(k = 0;k < 6;k++) {
            for(j = 0;j < 20;j++) {
                for(i = 0;i < this.semana[k].empleados.get(j).acts.size();i++){
                    //System.out.println(j + " - " + this.semana[k].empleados.get(j).acts.get(i).getNombre());
                    String turno = this.semana[k].empleados.get(j).getTurno() ? "Matutino" : "Vespertino" ;
                    datos[cont][k+2] = this.semana[k].empleados.get(j).getNombre() + " (" + turno +")";
                    cont++;
                }
                    
                //System.out.println(cont); 
            }
            cont=0;
        }
        
        return datos;
    }
    
    public String[][] getHorarioEmpleado(String nombre) {
        int i, j, k;
        
        String datos[][] = new String[46][8];
        
        for(k = 0;k < 6;k++) {
            for(j = 0;j < 20;j++) {
                
                for(i = 0;i < this.semana[k].empleados.get(j).acts.size();i++){

                    if (this.semana[k].empleados.get(j).getNombre().equals(nombre)) {
                        
                        datos = this.generarHoras(this.semana[k].empleados.get(j).getTurno(), datos);
                        
                        for (int l = 0; l < this.semana[k].empleados.get(j).actividades.length; l++) {
                            datos[l][k+1] = this.semana[k].empleados.get(j).actividades[l];
                        }
                    }
                        
                }
                
            }
        }
        
        return datos;
        
    }
    
    //Brandon
    private String[][] generarHoras(boolean turno, String[][] datos) {
        int hora = turno ? 8 : 14;
        String[][] horas = datos;
        for (int i = 0; i < 6; i++) {
            String zero = (hora + i < 10) ? "0" : "";
            String zero2 = (hora + i + 1 < 10) ? "0" : "";
            horas[i][0] = zero + (hora + i) + ":00 - "+ zero2 + (hora + i + 1) + ":00";
            //System.out.println(zero + (hora + i) + ":00 - "+ zero2 + (hora + i + 1) + ":00");
        }
        return horas;
        
    } 
    
    public Object[] getNombresEmpleado(){
        Object[] nombres = new Object[this.empleado.length];
        for (int i = 0; i < nombres.length; i++) {
            nombres[i] = this.empleado[i].getNombre();
        }
        return nombres;
    }
    //BrandonFin
    
    public boolean disponibleRegar(int hora, Actividad actividad) {
        return actividad.getNombre().compareTo("Regar areas verdes.") == 0 && hora == 0;
    }
    
    public boolean actividadRepetida(Actividad actividad, int index) {
        for(int i = 0;i < 6;i++) {
            if(this.empleados[index].actividades[i].compareTo(actividad.getNombre()) == 0)
                return true;
        }
        
        return false;
    }
    
    public boolean actividadChoca(int hora, int dia, Actividad actividad) {
        int cont = 0;
        for(int i = 0;i < this.semana[dia].empleados.size();i++) {
            if(this.semana[dia].empleados.get(i).actividades[hora].compareTo(actividad.getNombre()) == 0)
                cont++;
        }
        
        return cont > 0;
    }
    
    public int getHorasLibres(Empleado empleado) {
        return HORAS_TURNO - empleado.getHoras();
    }
    
    public boolean getSoloHombres(Actividad actividad) {
        if(actividad.getNombre().contains("Mantenimiento"))
            return true;
        else
            return false;
    }
}
