package proglogica;

/**
 *
 * @author dsk8
 */
public class Actividad {
    private String nombre;
    private int horas_trabajo;
    private boolean disponible;
    
    public Actividad(String nombre, int horas_trabajo) {
        this.nombre = nombre;
        this.horas_trabajo = horas_trabajo;
        this.disponible = true;
    }

    public String getNombre() {
        return nombre;
    }

    public int getHoras_trabajo() {
        return horas_trabajo;
    }
    
    public void setDisponible() {
        this.disponible = !disponible;
    }
    
    public boolean getDisponible() {
        return disponible;
    }
}
